﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiLibreria;

namespace FactuxD
{
    public partial class MantenimientoCliente : Mantenimiento
    {
        public MantenimientoCliente()
        {
            InitializeComponent();
        }

        public override Boolean Guardar()
        {
            try
            {
                string cmd = string.Format("EXEC ActualizarCliente'{0}','{1}','{2}'", txtIdCli.Text.Trim(),txtNomCli.Text.Trim(),txtApeCli.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("se ha guardado correctamente");
                return true;
            }
            catch (Exception error)
            {
                MessageBox.Show("ha ocurrido un error" + error.Message);
                return false;
            }
        }

        public override void Eliminar()
        {
            try
            {
                string cmd = string.Format("EXEC EliminarClientes'{0}'", txtIdCli.Text.Trim());
                Utilidades.Ejecutar(cmd);
                MessageBox.Show("se ha eliminado correctamente");
            }
            catch (Exception error)
            {
                MessageBox.Show("ha ocurrido un error " + error.Message);
            }
        }

        private void MantenimientoCliente_Load(object sender, EventArgs e)
        {

        }
    }
}
